#include<stdio.h>

void compareFiles(FILE *,FILE *);
void compareFiles(FILE *fp1, FILE *fp2){

    unsigned char buffer1[1025], buffer2[1025];
    int ret;
    int flag = 0;
    long x = 0;
    while((!feof(fp1)) && (!feof(fp2)) && flag != 1){
        while((fread(buffer1, 1024, 1, fp1) !=0) && (fread(buffer2, 1024, 1, fp2) !=0) && flag != 1){
            ret = memcmp(buffer1, buffer2, 1024);
            if(ret!=0){
                for(int i = 0; i < 1024; i++){
                    if(buffer1[i] != buffer2[i]){
                        x=x+i;
                        printf("Differ from: %li \n", x);
                        flag = 1;
                        break;
                    }
                }
            }
            else{
                    x=x+1024;
                }
        }
    }

    if(flag == 0){
        printf("Both Files Are Same \n");
    }
    else{
        printf("Files Are Not Same \n");
        fseek(fp1, x , SEEK_SET);
        int count = 0;
        while((count<16) && (!feof(fp1))){
            printf("%x \n", (unsigned)fgetc(fp1));
            count++;
        }
    }
}

void main(int argc, char *argv[]){

    FILE *fp1, *fp2;
    if(argc<3){
         printf("Insufficient Arguments \n");
         return;
    }
    else{
        fp1 = fopen(argv[1], "rb");
        if(fp1 == NULL){
            printf("Can't Open File 1 \n");
            return;
        }
        fp2 = fopen(argv[2], "rb");
        if(fp2 == NULL){
            printf("Can't Open File 2 \n");
            return;
        }
        compareFiles(fp1, fp2);
    }
}
